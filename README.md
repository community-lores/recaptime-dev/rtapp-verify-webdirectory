# Verification Endpoint Web Directory

An example Jekyll website for querying the Verification Endpoint API through the sourced YAML files in the `src/_data` directory.

**Status**: Under development, new project.

## Who to contact

* Maintainers:
  * Andrei Jiroh Halili <ajhalili2006@gmail.com> - Community Lores Project Coordinator, Recap Time squad member.

## Usage

1. Navigate to <http://directory.community-lores.gq/lookup> and select the following based on your needs:

  * **Content Creators** - Individuals who make content like journalists, developers and artists. Not necessary requires to be popular or be in the mainstream media as long as
  * **Communities** -
  * **Publishers** - Excluding rights management companies and soceities, these are entities (usually legal) who has TODO.

4. Enter the username that you want to look up. Press Enter and you'll be redirected there.

## Local development

1. Run `bundle install` to install Jekyll. Make sure your installation of Bundler is atleast `2.2.28` to avoid warnings regarding version mismatch.
2. If the data on `src/_data/indexes` become stale, run `scripts/sync-yaml-files`.
3. Run `scripts/dev` to run the dev server and open `http://localhost:4000` if the build succeeds.

## License

MIT
