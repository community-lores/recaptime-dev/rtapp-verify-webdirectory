---
layout: default
permalink: /404.html
title: We're f**ked.
---

## Sorry, but that page doesn't exist

Or, maybe that page has been moved without setting up any redirects or permanently deleted.
If you think this is an bug, please report this to the website maintainers using the **Report broken page** button below.

<a type="button" class="btn btn-secondary" href="{{ site.gitHost | default: 'https://gitlab.com' }}/{{site.namespace}}/{{site.repoSlug}}/issues/new">Report broken page</a>

**Didn't exepct this?** Check the URL for grammar issues and if the usage of dashes and forward-slashes are correct.

## Any context behind the 404 page title on the tab name?

It's an TheJuiceMedia reference regarding climate change, which the gas and oil industry still not accept, like in Australia.
