module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    "scope-enum": [
      2,
      "always",
      [
        "global",
        "vscode",
        "docs",
        "layouts",
        "assets",
        "jekyll-config",
        "yarn",
        "rubygems-bundler"
      ]
    ]
  }
}
